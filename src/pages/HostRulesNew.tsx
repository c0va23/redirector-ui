import Paper from '@material-ui/core/Paper'
import { WithStyles, withStyles } from '@material-ui/core/styles'
import * as log from 'loglevel'
import * as React from 'react'
import {
  RouteComponentProps,
  withRouter,
} from 'react-router-dom'
import {
  ConfigApiInterface,
  HostRules,
} from 'redirector-client'

import ButtonLink from '../components/ButtonLink'
import HostRulesFormWrapper, {
  ErrorSaveCb,
  SuccessSaveCb,
} from '../forms/HostRulesFormWrapper'

const logger = log.getLogger('HostRulesNew')

const styles = withStyles(theme => ({
  paper: {
    padding: theme.spacing(1),
    margin: theme.spacing(2),
  },
  backButton: {
    margin: theme.spacing(2),
  },
}))

interface Props {
  configApi: ConfigApiInterface,
}

class HostRulesNew extends React.Component<
  Props
  & RouteComponentProps<never>
  & WithStyles
  , HostRules
> {
  state = {
    host: '',
    defaultTarget: {
      path: '',
      httpCode: 301,
    },
    rules: [],
  }

  render () {
    return (
      <>
        <ButtonLink to='/host_rules_list' className={this.props.classes.backButton}>
          List
        </ButtonLink>

        <Paper className={this.props.classes.paper}>
          <h2>New rule host</h2>

          <HostRulesFormWrapper
            hostRules={this.state}
            onSaveHostRules={this.onSave}
            onUpdateHostRules={this.updateHostRules}
          />
        </Paper>
      </>
    )
  }

  private onSave = (
    _onSuccess: SuccessSaveCb,
    onError: ErrorSaveCb,
  ) =>
    this.props
      .configApi
      .createHostRules(this.state)
      .then(this.redirectToEditPage)
      .catch((error) => {
        logger.error(error)
        onError(error)
      })

  private updateHostRules = (hostRules: HostRules) =>
    this.setState(hostRules)

  private redirectToEditPage = (hostRules: HostRules): void =>
      this.props
        .history
        .push(`/host_rules_list/${hostRules.host}/edit`)
}

export default styles(
  withRouter(HostRulesNew),
)
