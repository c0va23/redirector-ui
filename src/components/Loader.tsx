import CircularProgress from '@material-ui/core/CircularProgress'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import withStyles, {
  WithStyles,
} from '@material-ui/core/styles/withStyles'
import * as React from 'react'

interface LoaderProps {
  label: string
}

const styles = withStyles({
  container: {
    flexGrow: 1,
  },
  item: {
    textAlign: 'center',
  },
})

class Loader extends React.PureComponent<LoaderProps & WithStyles> {
  render () {
    return (
      <Grid
        container
        justify='center'
        alignItems='center'
        alignContent='center'
        className={this.props.classes.container}
      >
        <Grid item className={this.props.classes.item}>
          <CircularProgress
            variant='indeterminate'
            size='60px'
          />
          <Typography variant='h5'>
            {this.props.label}
          </Typography>
        </Grid>
      </Grid>
    )
  }
}

export default styles(Loader)
