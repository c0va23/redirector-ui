import Button from '@material-ui/core/Button'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import withStyles, {
  WithStyles,
} from '@material-ui/core/styles/withStyles'
import * as React from 'react'
import {
  HostRules,
  Rule,
  Target,
} from 'redirector-client'

import ButtonLink from './ButtonLink'

export type OnDeleteHostRules = (host: string) => void

export interface HostRulesViewProps {
  hostRules: HostRules,
  onDelete: OnDeleteHostRules,
}

class HostRulesViewState {
  deletedEnabled: boolean = false
}

const styles = withStyles(theme => ({
  activeColumn: {},
  tableColumn: {
    paddingRight: theme.spacing(3),
  },
  ['@media screen and (max-width: 600px)']: {
    activeColumn: {
      display: 'none',
    },
  },
}))

class HostRulesView extends React.Component<
  HostRulesViewProps & WithStyles,
  HostRulesViewState
> {
  state = new HostRulesViewState()

  render () {
    let hostRules = this.props.hostRules
    return (
      <>
        {this.renderHostRules(hostRules)}
        {this.renderRules(hostRules.rules, hostRules.defaultTarget)}
      </>
    )
  }

  private renderHostRules = (hostRules: HostRules) => (
    <Toolbar>
      <Typography variant='h5' style={{ flex: 1 }}>
        {hostRules.host}
      </Typography>

      {this.renderDeleteButton()}
      <ButtonLink to={`/host_rules_list/${hostRules.host}/edit`}>
        Edit
      </ButtonLink>
    </Toolbar>
  )

  private renderDeleteButton () {
    if (!this.state.deletedEnabled) {
      return (
        <Button name='delete' onClick={this.enabledDelete}>
          Delete
        </Button>
      )
    }

    return (
      <Button name='delete' color='secondary' onClick={this.onDelete}>
        Delete
      </Button>
    )
  }

  private renderRules = (rules: Array<Rule>, defaultTarget: Target) => (
    <Table>
      <TableHead>
        <TableRow>
          <TableCell className={this.props.classes.tableColumn}>
            Source
          </TableCell>
          <TableCell className={this.props.classes.tableColumn}>
            Target (Code)
          </TableCell>
          <TableCell className={this.props.classes.tableColumn}>
            Target (Path)
          </TableCell>
          <TableCell className={this.props.classes.activeColumn}>
            Active from
          </TableCell>
          <TableCell className={this.props.classes.activeColumn}>
            Active to
          </TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {rules.map(this.renderRuleRow)}
        <TableRow title='Default target'>
          <TableCell className={this.props.classes.tableColumn}>*</TableCell>
          <TableCell className={this.props.classes.tableColumn}>{defaultTarget.httpCode}</TableCell>
          <TableCell className={this.props.classes.tableColumn}>{defaultTarget.path}</TableCell>
          <TableCell className={this.props.classes.activeColumn} />
          <TableCell className={this.props.classes.activeColumn} />
        </TableRow>
      </TableBody>
    </Table>
  )

  private renderRuleRow = (rule: Rule, index: number) => (
    <TableRow key={index}>
      <TableCell className={this.props.classes.tableColumn}>{rule.sourcePath}</TableCell>
      <TableCell className={this.props.classes.tableColumn}>{rule.target.httpCode}</TableCell>
      <TableCell className={this.props.classes.tableColumn}>{rule.target.path}</TableCell>
      <TableCell className={this.props.classes.activeColumn}>
        {rule.activeFrom && rule.activeFrom.toISOString()}
      </TableCell>
      <TableCell className={this.props.classes.activeColumn}>
        {rule.activeTo && rule.activeTo.toISOString()}
      </TableCell>
    </TableRow>
  )

  private enabledDelete = () =>
    this.setState({ deletedEnabled: true })

  private onDelete = (event: React.MouseEvent) => {
    event.preventDefault()
    this.props.onDelete(this.props.hostRules.host)
  }
}

export default styles(HostRulesView)
