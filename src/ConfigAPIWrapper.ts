import {
  ConfigApiInterface,
  HostRules,
  Locales,
  Rule,
} from 'redirector-client'

const parseDate = (date: string | undefined): Date | undefined => {
  if (date === undefined) return undefined
  return new Date(date)
}

const parseRule = (rule: Rule): Rule => ({
  ...rule,
  activeFrom: parseDate(rule.activeFrom as string | undefined),
  activeTo: parseDate(rule.activeTo as string | undefined),
})

const parseHostRules = (hostRules: HostRules): HostRules => ({
  ...hostRules,
  rules: hostRules.rules.map(parseRule),
})

export default class ConfigAPIWrapper implements ConfigApiInterface {
  configAPI: ConfigApiInterface

  constructor (configAPI: ConfigApiInterface) {
    this.configAPI = configAPI
  }

  createHostRules (hostRules: HostRules, options?: any): Promise<HostRules> {
    return this.configAPI.createHostRules(hostRules, options).then(parseHostRules)
  }

  deleteHostRules (host: string, options?: any): Promise<{}> {
    return this.configAPI.deleteHostRules(host, options)
  }

  getHostRule (host: string, options?: any): Promise<HostRules> {
    return this.configAPI.getHostRule(host, options).then(parseHostRules)
  }

  listHostRules (options?: any): Promise<Array<HostRules>> {
    return this.configAPI.listHostRules(options)
      .then(hostRules => hostRules.map(parseHostRules))
  }

  locales (options?: any): Promise<Locales> {
    return this.configAPI.locales(options)
  }

  updateHostRules (host: string, hostRules: HostRules, options?: any): Promise<HostRules> {
    return this.configAPI.updateHostRules(host, hostRules, options).then(parseHostRules)
  }
}
