import Button from '@material-ui/core/Button'
import FormControl from '@material-ui/core/FormControl'
import FormGroup from '@material-ui/core/FormGroup'
import InputLabel from '@material-ui/core/InputLabel'
import MenuItem from '@material-ui/core/MenuItem'
import Select from '@material-ui/core/Select'
import TextField from '@material-ui/core/TextField'
import withStyles, {
  WithStyles,
} from '@material-ui/core/styles/withStyles'
import { DateTimePicker, MaterialUiPickersDate } from '@material-ui/pickers'
import * as React from 'react'
import {
  ModelValidationError,
  Rule,
  Target,
} from 'redirector-client'

import AppContext, {
  AppContextData,
} from '../AppContext'
import SelectEventProps from '../utils/SelectEventProps'
import {
  buildLocalizer,
  findLocaleTranslations,
} from '../utils/localize'
import {
  embedValidationErrors,
  fieldValidationErrors,
} from '../utils/validationErrors'

import TargetForm from './TargetForm'

export type UpdateRule = (rule: Rule) => void
export type RemoveRule = () => void
export type MoveRule = () => void

interface Props {
  rule: Rule,
  ruleIndex: number,
  onUpdateRule: UpdateRule,
  onRemoveRule: RemoveRule,
  onMoveUpRule?: MoveRule,
  onMoveDownRule?: MoveRule,
  modelError: ModelValidationError,
}

const downCaseCharRegex = /[a-z]/

const styles = withStyles(theme => ({
  formControl: {
    marginTop: theme.spacing(2),
  },
  selectInput: {
    marginTop: theme.spacing(1),
  },
}))

class RuleForm extends React.Component<Props & WithStyles> {
  sourcePathRef = React.createRef<HTMLInputElement>()

  render () {
    return <AppContext.Consumer>{this.renderWithContext}</AppContext.Consumer>
  }

  private renderWithContext = (appContext: AppContextData) => {
    let classes = this.props.classes

    let locaeTranslations = findLocaleTranslations(appContext.errorLocales)
    let localizer = buildLocalizer(locaeTranslations)

    return (
      <FormGroup>
        <h3>Rule</h3>

        <TextField
          name='sourcePath'
          label='Source path'
          value={this.props.rule.sourcePath}
          onChange={this.onInputChange}
          className={classes.formControl}
          required
          error={this.fieldErrors('sourcePath').length > 0}
          helperText={this.fieldErrors('sourcePath').map(localizer).join(', ')}
          inputRef={this.sourcePathRef}
        />

        <FormControl
          className={classes.formControl}
          required
        >
          <InputLabel
            htmlFor={`rule-resolver-${this.props.ruleIndex}`}
          >
            Resolver
          </InputLabel>

          <Select
            name='resolver'
            value={this.props.rule.resolver}
            onChange={this.onSelectChange}
            inputProps={{ id: `rule-resolver-${this.props.ruleIndex}` }}
            className={this.props.classes.selectInput}
          >
            {this.resolverItems()}
          </Select>
        </FormControl>

        <DateTimePicker
          name='activeFrom'
          label='Active from'
          value={this.props.rule.activeFrom !== undefined ? this.props.rule.activeFrom : null}
          onChange={this.dateTimeChanger('activeFrom')}
          clearable
          ampm={false}
          helperText={this.fieldErrors('activeFrom').map(localizer).join(', ')}
          error={this.fieldErrors('activeFrom').length > 0}
          fullWidth
          className={classes.formControl}
        />

        <DateTimePicker
          name='activeTo'
          label='Active to'
          value={this.props.rule.activeTo !== undefined ? this.props.rule.activeTo : null}
          onChange={this.dateTimeChanger('activeTo')}
          clearable
          ampm={false}
          error={this.fieldErrors('activeTo').length > 0}
          helperText={this.fieldErrors('activeTo').map(localizer).join(', ')}
          fullWidth
          className={classes.formControl}
        />

        <h3>Target</h3>

        <TargetForm
          target={this.props.rule.target}
          onUpdateTarget={this.updateTarget}
          modelError={embedValidationErrors(this.props.modelError, 'target')}
        />

        <Button name='remove' onClick={this.props.onRemoveRule}>
          Remove
        </Button>

        {this.moveButton('up', 'Up', this.props.onMoveUpRule)}
        {this.moveButton('down', 'Down', this.props.onMoveDownRule)}
      </FormGroup>
    )
  }

  private moveButton = (name: string, text: string, cb?: MoveRule) => {
    if (cb === undefined) {
      return undefined
    }

    return (
      <Button
        name={name}
        onClick={cb}
      >
        {text}
      </Button>
    )
  }

  componentDidMount () {
    this.sourcePathRef.current!.focus()
  }

  private onInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    event.preventDefault()
    const name = event.target.name
    const value = event.target.value
    this.props.onUpdateRule({
      ...this.props.rule,
      [name]: value,
    })
  }

  private dateTimeChanger = (name: 'activeFrom' | 'activeTo') =>
    (newValue: MaterialUiPickersDate) =>
      this.props.onUpdateRule({
        ...this.props.rule,
        [name]: newValue,
      })

  private onSelectChange = (event: React.ChangeEvent<SelectEventProps>) => {
    event.preventDefault()
    const name = event.target.name as string
    const value = event.target.value as string
    this.props.onUpdateRule({
      ...this.props.rule,
      [name]: value,
    })
  }

  private updateTarget = (target: Target) =>
    this.props.onUpdateRule({
      ...this.props.rule,
      target,
    })

  private resolverItems = () =>
    Object.keys(Rule.ResolverEnum)
      .filter((resolver: string) => downCaseCharRegex.test(resolver[0]))
      .map((resolver) => (
        <MenuItem
          key={resolver}
          value={resolver}
        >
          {Rule.ResolverEnum[resolver as any]}
        </MenuItem>
      ))

  private fieldErrors = (fieldName: string) =>
    fieldValidationErrors(this.props.modelError, fieldName)
      .map(_ => _.translationKey)
}

export default styles(RuleForm)
