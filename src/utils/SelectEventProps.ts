export default interface SelectEventProps {
  name?: string,
  value: unknown,
}
