DOCKER_IMAGE := c0va23/redirector-ui
DOCKER_TAG := latest

clean:
	git clean -dxf **

gen/redirector-client/: api.yml
	docker run \
		--rm \
		-v ${PWD}:/app \
		-w /app \
		--user $(shell id -u) \
		swaggerapi/swagger-codegen-cli:2.4.8 \
		generate \
		-i api.yml \
		-l typescript-fetch \
		-D withInterfaces=true,npmName=redirector-client,supportsES6=true,npmRepository=gitlab.com/c0va23/redirector-ui \
		-o gen/redirector-client/

gen/redirector-client/dist/: gen/redirector-client/
	cd gen/redirector-client && npm install --no-save && npm run build

node_modules/: package.json package-lock.json gen/redirector-client/dist/
	npm ci

dist/index.html: node_modules/ src/ tsconfig.json webpack.config.js
	npm run build

test: node_modules/
	npm test

lint: node_modules/
	npm run lint

build-docker-image:
	docker build -t $(DOCKER_IMAGE):$(DOCKER_TAG) .
