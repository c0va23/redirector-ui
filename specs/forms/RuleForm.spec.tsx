/* tslint:disable:max-file-line-count */
import DayjsUtils from '@date-io/dayjs'
import {
  Button,
  InputLabel,
  Select,
  TextField,
} from '@material-ui/core'
import {
  DateTimePicker,
  MaterialUiPickersDate,
  MuiPickersUtilsProvider,
} from '@material-ui/pickers'
import dayjs from 'dayjs'
import {
  ReactWrapper,
  mount,
} from 'enzyme'
import * as React from 'react'
import {
  ModelValidationError,
  Rule,
  Target,
} from 'redirector-client'

import RuleForm, {
  MoveRule,
  RemoveRule,
  UpdateRule,
} from '../../src/forms/RuleForm'
import TargetForm from '../../src/forms/TargetForm'
import { randomDate } from '../factories/DateFactory'
import { randomPath } from '../factories/PathFactory'
import {
  randomResolver,
  randomRule,
} from '../factories/RuleFactory'
import { randomTarget } from '../factories/TargetFactory'

describe('RuleForm', () => {
  let rule: Rule
  let ruleIndex: number
  let updateRuleCb: UpdateRule
  let removeRuleCb: RemoveRule
  let moveUpRuleCb: MoveRule | undefined
  let moveDownRuleCb: MoveRule | undefined
  let modelError: ModelValidationError
  const ruleForm = () => mount(
    (
      <MuiPickersUtilsProvider utils={DayjsUtils}>
        <RuleForm
          rule={rule}
          ruleIndex={ruleIndex}
          onUpdateRule={updateRuleCb}
          onRemoveRule={removeRuleCb}
          onMoveUpRule={moveUpRuleCb}
          onMoveDownRule={moveDownRuleCb}
          modelError={modelError}
        />
      </MuiPickersUtilsProvider>
    ),
  )

  const fieldErrors = (fieldName: string) => [{
    name: fieldName,
    errors: [
      { translationKey: 'error1' },
      { translationKey: 'error2' },
    ],
  }]

  const fieldErrorMessage = 'error1, error2'

  const fireChangeInput = (
    field: ReactWrapper,
    name: keyof Rule,
    value: string,
  ) =>
    field
      .find('input')
      .simulate('change', {
        target: {
          name: name,
          value: value,
        },
      })

  beforeEach(() => {
    rule = randomRule()
    ruleIndex = Math.round(Math.random() * 10)
    updateRuleCb = jest.fn()
    removeRuleCb = jest.fn()
    moveUpRuleCb = undefined
    moveDownRuleCb = undefined
    modelError = []
  })

  describe('field sourcePath', () => {
    const fieldName = 'sourcePath'
    let sourcePathField = () =>
      ruleForm().find(TextField).filter({ name: fieldName }).first()

    it('have label', () => {
      expect(sourcePathField().prop('label')).toEqual('Source path')
    })

    it('have value', () => {
      expect(sourcePathField().prop('value')).toEqual(rule.sourcePath)
    })

    it('not have error', () => {
      expect(sourcePathField().prop('error')).toBeFalsy()
      expect(sourcePathField().prop('helperText')).toEqual('')
    })

    describe('update event', () => {
      let newSourcePath: string

      beforeEach(() => {
        newSourcePath = randomPath()
        fireChangeInput(sourcePathField(), fieldName, newSourcePath)
      })

      it('call update rule callback', () => {
        expect(updateRuleCb).toBeCalledWith({
          ...rule,
          sourcePath: newSourcePath,
        })
      })
    })

    describe('with errors', () => {
      beforeEach(() => modelError = fieldErrors(fieldName))

      it('have error', () => {
        expect(sourcePathField().prop('error')).toBeTruthy()
        expect(sourcePathField().prop('helperText')).toEqual(fieldErrorMessage)
      })
    })
  })

  const describeNullableDate = (fieldName: 'activeTo' | 'activeFrom', label: string) => {
    describe(`field ${fieldName}`, () => {
      let field = () =>
        ruleForm().find(DateTimePicker).filter({ name: fieldName }).first()

      it('have label', () => {
        expect(field().prop('label')).toEqual(label)
      })

      it('have value', () => {
        expect(field().prop('value')).toEqual(rule[fieldName])
      })

      it('not have error', () => {
        expect(field().prop('error')).toBeFalsy()
        expect(field().prop('helperText')).toEqual('')
      })

      describe('update event', () => {
        let newDate: MaterialUiPickersDate

        beforeEach(() => {
          newDate = dayjs(randomDate())
          field().prop('onChange')(newDate)
        })

        it('call update rule callback', () => {
          expect(updateRuleCb).toBeCalledWith({
            ...rule,
            [fieldName]: newDate,
          })
        })
      })

      describe(`when ${fieldName} is undefined`, () => {
        beforeEach(() => rule = { ...rule, [fieldName]: undefined })

        it('have empty string value', () => {
          expect(field().prop('value')).toEqual(null)
        })
      })

      describe('change to empty string', () => {
        beforeEach(() => field().prop('onChange')(null))

        it('call update rule callback with null activeTo', () => {
          expect(updateRuleCb).toBeCalledWith({ ...rule, [fieldName]: null })
        })
      })

      describe('with error', () => {
        beforeEach(() => modelError = fieldErrors(fieldName))

        it('have error', () => {
          expect(field().prop('error')).toBeTruthy()
          expect(field().prop('helperText')).toEqual(fieldErrorMessage)
        })
      })
    })
  }

  describeNullableDate('activeFrom', 'Active from')
  describeNullableDate('activeTo', 'Active to')

  describe('resolver selector', () => {
    const fieldName = 'resolver'
    let resolverSelect = () =>
      ruleForm().find(Select).filter({ name: fieldName }).first()

    it('have value', () => {
      expect(resolverSelect().prop('value')).toEqual(rule.resolver)
    })

    it('have label', () => {
      let inputProps = resolverSelect().props().inputProps!
      let inputLabel = ruleForm().find(InputLabel).filter({
        htmlFor: inputProps['id'],
      })
      expect(inputLabel.text()).toEqual(expect.stringContaining('Resolver'))
    })

    describe('on change', () => {
      let newResolver: Rule.ResolverEnum

      beforeEach(() => {
        newResolver = randomResolver()

        let onChange = resolverSelect().props().onChange!
        let event = {
          preventDefault: (): void => undefined,
          target: {
            name: fieldName,
            value: newResolver.toString(),
          },
        } as React.ChangeEvent<HTMLSelectElement>

        onChange(event, null as React.ReactNode)
      })

      it('call update rule callback', () => {
        expect(updateRuleCb).toBeCalledWith({ ...rule, resolver: newResolver })
      })
    })
  })

  describe('target fields', () => {
    let targetForm = () => ruleForm().find(TargetForm).first()

    it('have value', () => {
      expect(targetForm().prop('target')).toEqual(rule.target)
    })

    it('have empty model errors', () => {
      expect(targetForm().prop('modelError')).toEqual([])
    })

    describe('on update target', () => {
      let newTarget: Target

      beforeEach(() => {
        newTarget = randomTarget()
        targetForm().prop('onUpdateTarget')(newTarget)
      })

      it('call update rule callback', () => {
        expect(updateRuleCb).toBeCalledWith({
          ...rule,
          target: newTarget,
        })
      })
    })

    describe('with errors', () => {
      beforeEach(() => modelError = fieldErrors('target.httpCode'))

      it('have model error', () => {
        expect(targetForm().prop('modelError')).toEqual(fieldErrors('httpCode'))
      })
    })
  })

  describe('Remove button', () => {
    const removeButton = () => ruleForm().find(Button).filter({ name: 'remove' }).first()

    it('have text', () => {
      expect(removeButton().text()).toEqual('Remove')
    })

    describe('on click', () => {
      beforeEach(() => removeButton().simulate('click'))

      it('call remove rule callback', () => {
        expect(removeRuleCb).toBeCalled()
      })
    })
  })

  const describeMoveButton = (
    name: string,
    text: string,
    setMoveRuleCb: ((cb: MoveRule | undefined) => void),
  ) => {
    describe(`${text} button`, () => {
      const button = () => ruleForm().find(Button).filter({ name: name }).first()

      describe('when callback is undefined', () => {
        beforeEach(() => {
          setMoveRuleCb(undefined)
        })

        it('not render Up button', () => {
          expect(button()).toHaveLength(0)
        })
      })

      describe('when callback not undefined', () => {
        let moveCb: MoveRule
        beforeEach(() => {
          moveCb = jest.fn()
          setMoveRuleCb(moveCb)
        })

        it(`render button with text "${text}"`, () => {
          expect(button().text()).toEqual(text)
        })

        describe('on click', () => {
          beforeEach(() => button().simulate('click'))

          it('call up rule callback', () => {
            expect(moveCb).toBeCalled()
          })
        })
      })
    })

  }

  describeMoveButton('up', 'Up', (moveCb) => moveUpRuleCb = moveCb)
  describeMoveButton('down', 'Down', (moveCb) => moveDownRuleCb = moveCb)
})
